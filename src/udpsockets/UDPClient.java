
package udpsockets;

/**
 * Exemplo ilustrativo de comunicacao em rede com Sockets Java 
 * 
 * Autor: Luana Belusso
 * Ultima modificacao: 24/03/2018 21:15
 *
 */
import java.io.*;
import java.net.*;
import java.util.Random;

class UDPClient {

    public UDPClient() throws Exception {
        
        try{
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
        //API SOCKET
        DatagramSocket clientSocket = new DatagramSocket();
        // local do servidor
        InetAddress IPAddress = InetAddress.getByName("localhost");
        
        // formar pacote em bytes e enviar com tamanho 1024 bytes
        // Datagram é o pacote
        //UDP - USER DATAGRAM PROTOCOL
        //Envia cada pacote com um tamanho fixo
        byte[] sendData = new byte[1024];
        byte[] receiveData = new byte[1024];
        
        System.out.println("Cliente: ");
        
            for (int i=0; i<1000; i++){
                // lê a linha digitada e armazena
                //String sentence = inFromUser.readLine();
                String sentence = "teste x";
                // converte a linha digitada em bytes
                sendData = sentence.getBytes();

                //O socket é além de ip e porta, ele precisa fazer:
                //A classe Datagram Socket cria um SOCKET.
                //Um SOCKET é um canal de comunicação que tem os parametros: IP destino e Porta destino.
                //O objeto da classe possui operações de SEND (enviar) e RECENE (receber) datagramas.
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 9876);

                //Dados do cliente estao inteiramente contidos no pacote
                //Servidor usara os dados do pacote para responder no socket do cliente
                clientSocket.send(sendPacket);

                // envia
                //espera um tempo aleatorio antes de responder
                String resposta = setTimeout();
                
                System.out.println(resposta);                
                if (Integer.parseInt(resposta) < 2){
                    System.out.println("Timeout!");
                }else{
                    DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                    clientSocket.receive(receivePacket); //note que a recepção eh feita no socket cliente
                    String modifiedSentence = new String(receivePacket.getData());
                    System.out.println("FROM SERVER:" + modifiedSentence);
                }
            }
            
            clientSocket.close();
        }catch (Exception e){
                 e.printStackTrace();
                 System.out.println(e.getMessage());
         }
     }

    public static void main(String args[]) throws Exception{
        UDPClient minhaApp = new UDPClient();
    }
    
    public String setTimeout(){
        String resposta = "";
        try {
            Thread timeout = new Thread();
            Random tempo = new Random();
            int x = tempo.nextInt(10000);
            resposta = x + "";
            timeout.sleep(x);
        }catch (Exception e){
                e.printStackTrace();
                System.out.println(e.getMessage());
        }
        
        return resposta;
    }
}
