/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcpsockets;

/**
 * Exemplo ilustrativo de comunicacao em rede com Sockets Java 
 * 
 * Autor: Luana Belusso
 * Ultima modificacao: 24/03/2018 21:10
 *
 */

import java.io.*;
import java.net.*;
import java.util.Random;

class TCPClient {
    
    public TCPClient() throws Exception {
        
        try{
            String sentence;
            String modifiedSentence;
            BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
            Socket clientSocket = new Socket("localhost", 6789);

            for (int i=0; i<1000; i++){
                DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
                BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                
                sentence = inFromUser.readLine();
                //sentence = "teste x";
                outToServer.writeBytes(sentence + '\n');
                
                String resposta = setTimeout();
                
                System.out.println(resposta);                
                if (Integer.parseInt(resposta) < 2){
                    System.out.println("Timeout!");
                }else{
                    modifiedSentence = inFromServer.readLine();
                    System.out.println("FROM SERVER: " + modifiedSentence);
                }                
            }
            
            clientSocket.close();
        }catch (Exception e){
                 e.printStackTrace();
                 System.out.println(e.getMessage());
         }
    }
    
    public static void main(String argv[]) throws Exception {
         TCPClient minhaApp = new TCPClient();
    }
        
    public String setTimeout(){
        String resposta = "";
        try {
            Thread timeout = new Thread();
            Random tempo = new Random();
            int x = tempo.nextInt(10000);
            resposta = x + "";
            timeout.sleep(x);
        }catch (Exception e){
                e.printStackTrace();
                System.out.println(e.getMessage());
        }
        
        return resposta;
    }
}
